const firstName = document.querySelector('#first-name');
const lastName = document.querySelector ('#last-name');

const q1 = document.querySelector('#q1');
const a1 = document.querySelector('#a1');

const q2 = document.querySelector('#q2');
const a2 = document.querySelector('#a2');

const btn = document.querySelector('#submit-button');
btn.addEventListener('click', () => {
  let score = 0;

  if (q1.value === "wolf") {
    a1.innerHTML = 'Correct!'
    a1.style.color = 'green';
    score++;
  } else {
    a1.textContent = 'Wrong!'
    a1.style.color = 'red';
  };

  if (q2.value === "boy") {
    a2.innerHTML = 'Correct!'
    a2.style.color = 'green';
    score++;
  } else {
    a2.textContent = 'Wrong!'
    a2.style.color = 'red';
  };

  alert('Good day, ' + firstName.value + ' ' + lastName.value + ' . You have '+ score + '/2 questions correct');
})











































